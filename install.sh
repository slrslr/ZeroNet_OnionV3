#!/bin/bash 
if [ -d $PWD/../src/ ]
then
    sed -i '/from Crypt import CryptRsa/a from Crypt import CryptEd25519' $PWD/../src/Tor/TorManager.py
    sed -i '/from Crypt import CryptRsa/a from Crypt import CryptEd25519' $PWD/../plugins/AnnounceZero/AnnounceZeroPlugin.py
    sed -i 's/RSA1024/ED25519-V3/g' $PWD/../src/Tor/TorManager.py
    cp $PWD/../src/Crypt/CryptRsa.py $PWD/backup_file/CryptRsa.py
    cp $PWD/upgrade_file/CryptRsa.py $PWD/../src/Crypt/CryptRsa.py
    cp $PWD/upgrade_file/CryptEd25519.py $PWD/../src/Crypt/CryptEd25519.py
    echo "Good, all done!"
else
    echo "! I was not executed in the right place"
fi    
