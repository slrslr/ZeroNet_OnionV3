## main reference:
- https://zeronet.io
	- V3 mirror http://zeronet34m3r5ngdu54uj57dcafpgdjhxsgq5kla5con4qvcmfzpvhad.onion
- https://github.com/HelloZeroNet/ZeroNet

# V3 Hidden Service support
sadly script with small patch to integrate support for hidden (onion) service V3 on ZeroNet darknet ( still support V2 ).

this patch refers these issue:
- https://github.com/HelloZeroNet/ZeroNet/issues/2351
- https://github.com/HelloZeroNet/ZeroNet/issues/1292
- or from ZeroNet [GitCenter](http://127.0.0.1:43110/1GitLiXB6t5r8vuU2zC6a8GYj9ME6HMQ4t/repo/issues/view/?1P4w4Rvh4vS9r6G5xTiSh8qEW87EKZtnJB/9@1DfrA2M9Qra6stqT6tzwNMYNweswY54KAC)

In particular it manages the tracker function ( bootstrap plugin enable ) verifying and distributing onion V3 peers to others ( to both patched and unpatched clients ).

As you may know the hiddien service V2 are deprecated and in October will cease to exist permanently, is why in the transition phase (wait for V3 support to be released from the official repo) it's important to enable the boostrap plugin (and add v3 trackers in bootstrap list) to verify and share OnionV3 peers that cannot be verified by current trackers with the stable version.

### Patch explained
This patch doesn't definitively differentiate onion v2 from v3, via `CryptRsa.py` I've maintained the v2 functions unchanged and added a copy of those funcionts ( sign, verify key .. ) with library required by onion v3 (`CryptEd25519.py`), recognising the type of address version from the length of the hidden service keys used in the functions ( **definitely could be improved!** ).

### Script explained
- move `upgrade_file/CryptEd25519.py` and `upgrade_file/CryptRsa.py` to `ZeroPath/src/Crypt/` 
- replace in `ZeroPath/src/Tor/TorManager.py` the 2 words `RSA1024` with `ED25519-V3`
- add new line `from Crypt import CryptEd25519` to the top of the files:
	- `ZeroPath/src/Tor/TorManager.py`
	- `ZeroPath/plugins/AnnounceZero/AnnounceZeroPlugin.py`

---

For the installation guide i will use a fresh new client, following the installation guide on the official repo : https://github.com/HelloZeroNet/ZeroNet

#### Linux (x86-64bit)
- `wget https://github.com/HelloZeroNet/ZeroNet-linux/archive/dist-linux64/ZeroNet-py3-linux64.tar.gz`
- `tar xvpfz ZeroNet-py3-linux64.tar.gz`
- `cd ZeroNet-linux-dist-linux64/`

now move inside core/ dir and download this repo
- `cd core`
- `git clone https://git.disroot.org/anonymoose/ZeroNet_OnionV3.git`

move inside downloaded folder and launch `install.sh` script.
- `cd ZeroNet_OnionV3`
- `./install.sh`
back to main folder
- `cd ../..`

now the client is enabled to support V3 hidden services
you can launch the client normally from ZeroNet-linux-dist-linux64
- `./ZeroNet.sh`

#### Tor configuration and tracker settings
if you want to start immediately in tracker - tor always mode these are configurations tested on debian10, more information can be found in the official FAQ:
- https://zeronet.io/docs/faq/#how-to-use-zeronet-with-the-tor-browser
- https://zeronet.io/docs/faq/#how-to-use-zeronet-with-tor

for the use described the commands to follow for the configuration of Tor are described here:
https://zeronet.io/docs/faq/#how-to-make-zeronet-work-with-tor-under-linuxmacos

- `sudo nano /etc/tor/torrc`
- remove `#` character from lines `ControlPort 9051` and `CookieAuthentication 1`
- restart tor : `sudo service tor restart`
- add permission for yourself to read the auth cookie. with Debian Linux, the command is `sudo usermod -a -G debiantor [yourlinuxuser]` (if you are not on Debian check the file's user group by `ls -al /var/run/tor/control.authcookie`)
- logout/login with your user to apply group changes

- add a `zeronet.conf` on ZeroNet-linux-dist-linux64 with
```
[global]
tor = always 
```
- enable tracker plugin, always from ZeroNet-linux-dist-linux64 
	`mv core/plugins/disabled-Bootstrapper core/plugins/Bootstrapper`
- launch `./ZeroNet.sh` 
- open http://127.0.0.1:43110/Stats to check the operation

---

### Tips
OnionV3 tracker to add in bootstrap list:
- `zero://tnjppdoc4famqa62mldlzc3fqw2wlsfxov7iz3pz5ex6umnqpaupflqd.onion:16227`

Syncronite trackers file list :
- http://127.0.0.1:43110/15CEFKBRHFfAP9rmL6hhLmHoXrrgmw4B5o
---

### Todo
- understand why onion addresses aren't shared as shared trackers
- permanent tracker onion address
- upgrade plugins/StemPort for V3 support
- default stem lib for manage hidden services
