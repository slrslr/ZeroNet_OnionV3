#!/bin/bash 
if [ -d $PWD/../src/ ]
then
    sed -i '/from Crypt import CryptEd25519/d' $PWD/../src/Tor/TorManager.py
    sed -i '/from Crypt import CryptEd25519/d' $PWD/../plugins/AnnounceZero/AnnounceZeroPlugin.py
    sed -i 's/ED25519-V3/RSA1024/g' $PWD/../src/Tor/TorManager.py
    cp $PWD/backup_file/CryptRsa.py $PWD/../src/Crypt/CryptRsa.py
    rm $PWD/../src/Crypt/CryptEd25519.py
    echo "Good, all restored!"
else
    echo "! I was not executed in the right place"
fi    
